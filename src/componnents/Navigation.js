import React from 'react';
import {NavLink} from "react-router-dom";
import styled from "styled-components";
import DarkLogo from '../assets/dark-logo.png'

import { StyledLink } from '../styles/Atoms'


const NavContainer = styled.nav`
  padding: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const HomeLogo = styled.img`
  height: 70px;
`


const Navigation = () => {
    return (
<NavContainer>
          <NavLink to="/">
        <HomeLogo src={DarkLogo} />
      </NavLink>
        <div>
            <StyledLink exact to="/"  activeClassName="nav-active">
                Accueil
            </StyledLink>

            <StyledLink exact to="/survey/1" $isFullLink activeClassName="nav-active" >
                Questionnaire
            </StyledLink>

              <StyledLink exact to="/freelances" activeClassName="nav-active">
                Profils
            </StyledLink>

        </div>
       </NavContainer>
    );
};

export default Navigation;