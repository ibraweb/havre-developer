import DefaultPicture from '../assets/profile.png'

const freelanceProfiles = [
    {
        name: 'Ibrahim',
        jobTitle: 'Devops',
        picture: DefaultPicture,
    },
    {
        name: 'Ismaël',
        jobTitle: 'Developpeur frontend',
        picture: DefaultPicture,
    },

     {
        name: 'Zadjirati',
        jobTitle: 'Développeuse Fullstack',
        picture: DefaultPicture,
    },
]
export default freelanceProfiles;