import React from 'react';
import {BrowserRouter, useRouteMatch, Route, Switch} from "react-router-dom";
import Dashboard from "./Dashboard";
import Settings from "./Settings";

const Admin = () => {
    let { path } = useRouteMatch()
    return (
        <div>
            <h1>Espace Admin du site</h1>

                <Switch>
                    <Route exact path={`${path}/dashboard`}>
                        <Dashboard />
                    </Route>
                    <Route exact path={`${path}/settings`} component={Settings} />
                </Switch>
        </div>
    );
};

export default Admin;