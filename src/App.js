import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./pages/Home";
import Survey from "./pages/Survey";
import NoteFound from "./pages/NoteFound";
import Admin from "./pages/Admin";
import User from "./pages/User";
import Result from "./pages/Result";
import Freelances from "./pages/Freelances";


function App() {
  return (
  <BrowserRouter>
      <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/survey/:questionNumber" exact component={Survey} />
          <Route path="/admin" exact component={Admin} />
          <Route path="/user" exact component={User} />
          <Route path="/results" exact component={Result} />
          <Route path="/freelances" exact component={Freelances} />
          <Route component={NoteFound} />
      </Switch>

  </BrowserRouter>
  );
}

export default App;
